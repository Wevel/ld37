﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHireWorker : MonoBehaviour {

	public MapControl mapControl;
	public Text buttonText;

	void Update ()
	{
		buttonText.text = "Hire Worker: " + mapControl.NextHireCost () + PlayerBank.CurrencySymbol;
	}

	public void Hire ()
	{
		mapControl.TryHireWorker ();
	}
}

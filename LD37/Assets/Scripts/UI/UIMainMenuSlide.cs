﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainMenuSlide : MonoBehaviour {
	public float inPosition;
	public float outPosition;

	public float moveSpeed = 5;

	public bool isIn = false;

	private RectTransform rt;

	void Awake ()
	{
		rt = GetComponent<RectTransform> ();

		//if (isIn)
		//	rt.position = new Vector3 (rt.position.x, Screen.height + outPosition);
		//else
		//	rt.position = new Vector3 (rt.position.x, Screen.height + inPosition);
	}

	void Update ()
	{
		rt.position = Vector2.Lerp (rt.position, isIn ? new Vector3 (rt.position.x, Screen.height + inPosition) : new Vector3 (rt.position.x, Screen.height + outPosition), moveSpeed * Time.deltaTime);
	}

	public void Toggle ()
	{
		isIn = !isIn;
	}

	public void Set (bool value)
	{
		isIn = value;
	}
	
	public bool IsIn
	{
		get
		{
			return isIn;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public float panSpeed = 5;
	public float zoomSpeed = 1;

	public float minZoom = 3;
	public float maxZoom = 20;

	private Vector3 lastPosition;

	void Awake ()
	{
		DontDestroyOnLoad (gameObject);
	}

	void Update ()
	{
		if (!MainMenu.Playing) return;

		if (!Camera.main.pixelRect.Contains (Input.mousePosition)) return;

		Vector3 currentPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		if (Input.GetMouseButton (2))
		{
			if (Input.GetMouseButtonDown (2)) lastPosition = currentPos;

			Camera.main.transform.Translate(lastPosition - currentPos);
		}

		lastPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		Camera.main.orthographicSize -= Camera.main.orthographicSize * Input.GetAxis ("Mouse ScrollWheel") * zoomSpeed;
		Camera.main.orthographicSize = Mathf.Clamp (Camera.main.orthographicSize, minZoom, maxZoom);
	}
}

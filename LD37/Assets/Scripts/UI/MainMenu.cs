﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	private static MainMenu instance;

	public static bool Playing
	{
		get
		{
			return instance.paused;
		}
	}

	public string mainSceneName;

	public UIMainMenuSlide mainMenuSlide;
	public UIMainMenuSlide winGameSlide;

	public GameObject playButton;
	public GameObject mainMenuButton;

	public GameObject mainMenuTag;

	bool playing = false;
	bool paused = false;

	void Awake ()
	{
		DontDestroyOnLoad (gameObject);

		instance = this;
	}

	void Update ()
	{
		paused = mainMenuSlide.IsIn || !winGameSlide.IsIn;

		playButton.SetActive (!playing && winGameSlide.IsIn);
		mainMenuButton.SetActive (playing && winGameSlide.IsIn);
		mainMenuTag.SetActive (playing && winGameSlide.IsIn);
	}

	public void StartGame ()
	{
		SceneManager.LoadScene (mainSceneName);

		mainMenuSlide.Set (true);
		winGameSlide.Set (true);

		playing = true;
	}

	public void EndGame ()
	{
		SceneManager.LoadScene ("Empty");

		mainMenuSlide.Set (false);
		winGameSlide.Set (true);

		playing = false;
	}

	public void QuitGame ()
	{
		Application.Quit ();
	}

	public static void WinGame ()
	{
		instance.winGameSlide.Set (false);
	}
}

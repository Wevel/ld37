﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIProductionLine : MonoBehaviour {

	public Text typeText;
	public Text valueText;

	public ProductionTarget target;

	public Image crossOutLine;

	void Start ()
	{
		ItemType type = ItemTypesLoader.GetItemType (target.typekey);
		typeText.text = "-" + type.name + ":";
	}

	void Update () {
		valueText.text = target.currentAmount + "/" + target.targetAmount;

		crossOutLine.gameObject.SetActive (target.currentAmount >= target.targetAmount);
	}
}

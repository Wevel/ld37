﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBuildMenuSlide : MonoBehaviour {
	public float inPosition;
	public float outPosition;

	public float moveSpeed = 5;

	private bool isIn = true;

	private RectTransform rt;

	void Awake ()
	{
		rt = GetComponent<RectTransform> ();

		rt.position = new Vector3 (inPosition, rt.position.y);
	}

	void Update ()
	{
		rt.position = Vector2.Lerp (rt.position, isIn ? new Vector3 (inPosition, rt.position.y) : new Vector3 (outPosition, rt.position.y), moveSpeed * Time.deltaTime);
	}

	public void Toggle ()
	{
		isIn = !isIn;
	}

	public void Set (bool value)
	{
		isIn = value;
	}
}

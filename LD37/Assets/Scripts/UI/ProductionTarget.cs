﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProductionTarget
{
	public string typekey;
	public int targetAmount;

	[System.NonSerialized]
	public int currentAmount;
}

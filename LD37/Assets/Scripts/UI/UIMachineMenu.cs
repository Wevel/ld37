﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMachineMenu : MonoBehaviour {

	public UIBuildMenuSlide machineMenuSlider;
	public UIBuildMenuSlide buildMenuSlider;

	public Text machineNameText;
	public Toggle buyInputsToggle;
	public Toggle sellOutputsToggle;
	public Dropdown recipeSelectDropDown;

	private Machine currentMachine;

	private List<Recipe> possibleRecipes;

	public void Show (Machine machine, Sprite positionDisplaySprite, Color inColour, Color outColour)
	{
		if (currentMachine != null) currentMachine.HideInOutPositions ();

		if (machine == null)
		{
			Hide ();
			return;
		}

		currentMachine = machine;

		currentMachine.ShowInOutPositions (positionDisplaySprite, inColour, outColour);

		possibleRecipes = ItemTypesLoader.FindPossibleRecipies (currentMachine.type);

		recipeSelectDropDown.ClearOptions ();

		recipeSelectDropDown.AddOptions (new List<string> (new string[] { "None" }));
		foreach (Recipe item in possibleRecipes)
		{
			recipeSelectDropDown.AddOptions (new List<string> (new string[] { item.product.name }));
		}

		recipeSelectDropDown.value = possibleRecipes.IndexOf (currentMachine.currentRecipe) + 1;

		machineMenuSlider.Set (false);
		buildMenuSlider.Set (true);

		machineNameText.text = currentMachine.type.name;

		buyInputsToggle.isOn = currentMachine.buyInputs;
		sellOutputsToggle.isOn = currentMachine.sellOutputs;
	}

	public void Hide ()
	{
		machineMenuSlider.Set (true);
		if (currentMachine != null) currentMachine.HideInOutPositions ();
	}

	public void ToggleBuyInputs ()
	{
		if (currentMachine == null) return;

		currentMachine.buyInputs = buyInputsToggle.isOn;
	}

	public void ToggleSellOutputs ()
	{
		if (currentMachine == null) return;

		currentMachine.sellOutputs = sellOutputsToggle.isOn;
	}

	public void ChageDropDownRecipe ()
	{
		if (currentMachine == null || possibleRecipes == null || possibleRecipes.Count == 0) return;

		if (recipeSelectDropDown.value - 1 == -1)
		{
			currentMachine.currentRecipe = null;
		}
		else
		{
			currentMachine.currentRecipe = possibleRecipes[Mathf.Clamp (recipeSelectDropDown.value - 1, 0, possibleRecipes.Count)];
		}
	}

	public void SellMachine ()
	{
		if (currentMachine == null) return;

		currentMachine.Sell ();
		Hide ();
	}
}

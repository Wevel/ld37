﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBuildMenu : MonoBehaviour
{
	public MachineTypeButton machineTypeButtonPrefab;

	public MachinePlacer placer;

	public UIBuildMenuSlide uiBuildMenuSlider;

	void Start ()
	{
		MachineTypeButton currentType;

		foreach (MachineType item in MachineTypesLoader.MachineTypes)
		{
			currentType = Instantiate (machineTypeButtonPrefab, transform);
			currentType.type = item;
			currentType.placer = placer;
			currentType.uiBuildMenuSlider = uiBuildMenuSlider;
		}
	}
}

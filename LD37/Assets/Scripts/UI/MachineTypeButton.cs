﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MachineTypeButton : MonoBehaviour
{
	public UIBuildMenuSlide uiBuildMenuSlider;

	public MachinePlacer placer;

	public Text typeNameText;
	public Text typeCostText;
	public Image typeIcon;

	private MachineType _type = null;
	public MachineType type
	{
		get
		{
			return _type;
		}
		set
		{
			_type = value;

			if (_type != null)
			{
				typeNameText.text = _type.name;
				typeCostText.text = _type.buyCost + "" + PlayerBank.CurrencySymbol;
				typeIcon.sprite = _type.sprite;
			}
		}
	}

	public void Select ()
	{
		if (type == null) return;
		placer.currentMachineType = type;
		uiBuildMenuSlider.Toggle ();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionManager : MonoBehaviour
{
	public UIProductionLine productionLinePrefab;

	public Transform headerText;

	public List<ProductionTarget> targets = new List<ProductionTarget> ();

	bool doneWin = false;

	void Start ()
	{
		UIProductionLine line;

		foreach (ProductionTarget item in targets)
		{
			line = Instantiate (productionLinePrefab);
			line.target = item;
			line.transform.SetParent (transform);
		}

		headerText.SetParent (transform);
		headerText.SetSiblingIndex (0);
	}

	public void soldItem (ItemType type)
	{
		ProductionTarget target = targets.Find (x => x.typekey == type.key && x.currentAmount < x.targetAmount);

		if (target != null)
		{
			target.currentAmount++;
			if (target.currentAmount > target.targetAmount) target.currentAmount = target.targetAmount;
		}

		bool metAllTargets = true;

		foreach (ProductionTarget item in targets)
		{
			if (item.currentAmount < item.targetAmount) metAllTargets = false;
		}

		if (metAllTargets)
		{
			if (!doneWin)
			{
				Debug.Log ("Met all targets");
				MainMenu.WinGame ();

				doneWin = true;
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MachinePlacer : MonoBehaviour {

	public MapControl mapControl;

	public UIMachineMenu machineMenu;

	public MachineType currentMachineType;

	public SpriteRenderer buildDisplay;
	public SpriteRenderer buildInputDisplay;
	public SpriteRenderer buildOutputDisplay;
	public Color validPlaceColour = Color.white;
	public Color invalidPlaceColour = Color.red;
	public Color invalidMoneyColour = Color.yellow;

	public Sprite positionDisplaySprite;
	public Color inColour;
	public Color outColour;

	void Update ()
	{
		if (mapControl != null && mapControl.room != null)
		{
			Vector2 pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			Tile tile = mapControl.room.GetTile (pos);

			// Have a machine type to place
			if (currentMachineType != null)
			{
				buildInputDisplay.transform.localPosition = new Vector3 (currentMachineType.inputOffset.x, currentMachineType.inputOffset.y);
				buildOutputDisplay.transform.localPosition = new Vector3 (currentMachineType.outputOffset.x, currentMachineType.outputOffset.y);

				buildDisplay.sprite = currentMachineType.sprite;

				if (tile != null)
				{
					if (mapControl.room.CanPlaceMachine (tile, currentMachineType))
					{
						buildDisplay.color = validPlaceColour;
					}
					else
					{
						if (PlayerBank.CanBuy (currentMachineType))
						{
							buildDisplay.color = invalidPlaceColour;
						}
						else
						{
							buildDisplay.color = invalidMoneyColour;
						}
					}

					buildDisplay.transform.position = tile.GetTrueLocation ();
				}
				else
				{
					buildDisplay.color = invalidPlaceColour;

					buildDisplay.transform.position = new Vector2(Mathf.RoundToInt (pos.x), Mathf.RoundToInt (pos.y));
				}

				buildDisplay.gameObject.SetActive (true);

				Machine[] machines = FindObjectsOfType<Machine> ();

				for (int i = 0; i < machines.Length; i++)
				{
					machines[i].ShowInOutPositions (positionDisplaySprite, inColour, outColour);
				}

				if (Input.GetMouseButtonDown (1))
				{
					currentMachineType = null;

					for (int i = 0; i < machines.Length; i++)
					{
						machines[i].HideInOutPositions ();
					}
				}
				else if (Input.GetMouseButtonDown (0))
				{
					if (!EventSystem.current.IsPointerOverGameObject())
						mapControl.room.PlaceMachine (tile, currentMachineType, mapControl.machineMaterial);
				}
			}
			else
			{
				buildDisplay.gameObject.SetActive (false);

				if (tile != null)
				{
					if (Input.GetMouseButtonDown (0))
					{
						if (!EventSystem.current.IsPointerOverGameObject ())
						{
							machineMenu.Show (mapControl.room.machineManager.GetMachine (tile), positionDisplaySprite, inColour, outColour);
						}

					}
				}
				else
				{
					if (Input.GetMouseButtonDown (0))
					{
						if (!EventSystem.current.IsPointerOverGameObject ())
							machineMenu.Hide ();
					}
				}
			}			
		}
	}

	public void SetMachineBuildType (string key)
	{
		currentMachineType = MachineTypesLoader.GetMachineType (key);
	}
}

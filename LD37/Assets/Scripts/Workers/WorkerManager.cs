﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerManager
{
	private Transform workerHolder;

	public WorkerManager ()
	{
		workerHolder = new GameObject ("WorkerHolder").transform;
		workerHolder.transform.position = Vector3.zero;
	}

	public Worker SpawnWorker (Room room, Tile tile, Direction spawnDirection, GetWorkerSprite getWorkerSprite, Material material, GetWorkerTasks getWorkerTasks, float movementSpeed)
	{
		Worker worker = new GameObject ("Worker").AddComponent<Worker> ();
		worker.transform.SetParent (workerHolder);
		worker.transform.position = tile.GetTrueLocation ();

		SpriteRenderer sr = worker.gameObject.AddComponent<SpriteRenderer> ();
		sr.sprite = getWorkerSprite (spawnDirection);
		sr.material = material;
		sr.sortingOrder = 10;

		worker.Init (room, getWorkerSprite, getWorkerTasks, movementSpeed);

		return worker;
	}
}

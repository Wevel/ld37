﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour, IPosition {

	#region Held Item Position

	private readonly Vector2 faceUpItemPosition = new Vector2 (0, 0.04f);
	private readonly Vector2 faceDownItemPosition = new Vector2 (0, -0.125f);
	private readonly Vector2 faceRightItemPosition = new Vector2 (0.218f, -0.094f);
	private readonly Vector2 faceLeftItemPosition = new Vector2 (-0.218f, -0.094f);

	private readonly int faceUpItemSortOrder = 9;
	private readonly int faceDownItemSortOrder = 11;
	private readonly int faceRightItemSortOrder = 11;
	private readonly int faceLeftItemSortOrder = 11;

	#endregion

	public Room room { get; private set; }

	private GetWorkerSprite getWorkerSprite;
	//private GetWorkerTasks getWorkerTasks;

	public float movementSpeed { get; private set; }

	private SpriteRenderer sr;

	private TaskManager manger;

	private Vector2 lastPosition;

	public ItemType heldItem;

	private SpriteRenderer heldItemDislay;

	public List<Tile> debugPath;

	void Awake ()
	{
		lastPosition = transform.position;

		heldItem = null;

		heldItemDislay = new GameObject ("HeldDispaly").AddComponent<SpriteRenderer> ();
		heldItemDislay.sortingOrder = 11;
		heldItemDislay.enabled = false;
		heldItemDislay.transform.SetParent (transform);
		heldItemDislay.transform.position = Vector3.zero;
	}

	void Update ()
	{
		if (MainMenu.Playing)
			if (manger != null) manger.UpdateTasks (room.timeScale * Time.deltaTime);

		Vector2 delta = (Vector2)transform.position - lastPosition;

		if (Mathf.Abs (delta.x) > Mathf.Abs (delta.y))
		{
			delta.y = 0;
		}
		else if (Mathf.Abs (delta.y) > Mathf.Abs (delta.x))
		{
			delta.x = 0;
		}

		UpdateDirection (Tile.GetDirection(delta));

		if (heldItem != null)
		{
			heldItemDislay.sprite = heldItem.sprite;
			heldItemDislay.enabled = true;
		}
		else
		{
			heldItemDislay.enabled = false;
		}

		lastPosition = transform.position;
	}

	public void Init (Room room, GetWorkerSprite getWorkerSprite, GetWorkerTasks getWorkerTasks, float movementSpeed)
	{
		sr = GetComponent<SpriteRenderer> ();

		this.room = room;
		this.getWorkerSprite = getWorkerSprite;

		//this.getWorkerTasks = getWorkerTasks;

		this.movementSpeed = movementSpeed;

		manger = new TaskManager (this, getWorkerTasks);
	}

	public Tile GetTileLocation ()
	{
		Vector2 pos = transform.position;

		return room.GetTile (pos);
	}

	public Vector2 GetTrueLocation ()
	{
		return transform.position;
	}

	public void UpdateDirection (Direction dir)
	{
		if (dir != 0) sr.sprite = getWorkerSprite (dir);

		switch (dir)
		{
			case Direction.Up:
				heldItemDislay.transform.localPosition = faceUpItemPosition;
				heldItemDislay.sortingOrder = faceUpItemSortOrder;
				break;
			case Direction.Right:
				heldItemDislay.transform.localPosition = faceRightItemPosition;
				heldItemDislay.sortingOrder = faceRightItemSortOrder;
				break;
			case Direction.Down:
				heldItemDislay.transform.localPosition = faceDownItemPosition;
				heldItemDislay.sortingOrder = faceDownItemSortOrder;
				break;
			case Direction.Left:
				heldItemDislay.transform.localPosition = faceLeftItemPosition;
				heldItemDislay.sortingOrder = faceLeftItemSortOrder;
				break;
		}
	}

	//void OnDrawGizmos ()
	//{
	//	if (debugPath != null && debugPath.Count > 1)
	//	{
	//		Gizmos.color = Color.blue;

	//		for (int i = 0; i < debugPath.Count - 1; i++)
	//		{
	//			Gizmos.DrawLine (debugPath[i].GetTrueLocation (), debugPath[i + 1].GetTrueLocation ());
	//			Gizmos.color = Color.red;
	//		}
	//	}
	//}
}

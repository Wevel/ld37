﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskManager
{
	private Queue<Task> taskQueue = new Queue<Task> ();

	public Task currentTask { get; private set; }

	public Worker worker;
	private GetWorkerTasks onGetWorkerTasks;

	public bool interupted { get; private set; }

	public TaskManager (Worker worker, GetWorkerTasks onGetWorkerTasks)
	{
		this.worker = worker;
		this.onGetWorkerTasks = onGetWorkerTasks;

		NextTask ();
	}

	public void UpdateTasks (float deltaTime)
	{
		interupted = false;

		if (currentTask != null)
		{
			currentTask.Update (this, deltaTime);
		}
		else
		{
			NextTask ();
		}
	}

	public void NextTask ()
	{
		if (taskQueue.Count > 0)
		{
			currentTask = taskQueue.Dequeue ();

			currentTask.Start (this);
		}
		else
		{
			// No tasks in queue, go get some more
			taskQueue = new Queue<Task> (onGetWorkerTasks (worker));
			NextTask ();
		}
	}

	public void InterruptTask (params Task[] newTasks)
	{
		interupted = true;

		Queue<Task> newTaskQueue = new Queue<Task> (newTasks);

		if (currentTask != null)
		{
			currentTask.Pause (this);

			newTaskQueue.Enqueue (currentTask);
		}

		while (taskQueue.Count > 0) newTaskQueue.Enqueue (taskQueue.Dequeue ());

		taskQueue = newTaskQueue;
		
		NextTask ();
	}

	public void InterruptMoveTo (IPosition position)
	{
		InterruptTask (Task.MoveTo (position));
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task {

	const float resourceInteractionDelay = 1;
	const float resourceWaitForRetryTime = 0.25f;

	#region Standard Tasks

	public static Task MoveTo (IPosition targetPosition, TaskSuccessful onSuccess = null)
	{
		List<Tile> path = null;
		Tile currentTile = null;

		return new Task (
			onStart: (manager) =>
			{
				if (currentTile == null || path == null || path.Count <= 0)
				{
					path = Pathfinding.GetPath (manager.worker.room, manager.worker, targetPosition);

					if (path == null || path.Count <= 0)
					{
						// No path

						Debug.LogError ("No path to target");
						return;
					}
				}

				manager.worker.debugPath = path;
				currentTile = manager.worker.GetTileLocation ();
			},

			onPause: (manager) =>
			{
				manager.worker.debugPath = null;
				currentTile = manager.worker.GetTileLocation ();
			},

			onUpdate: (manager, deltaTime) =>
			{
				// If we are at the tile we can finish the tast
				if (Tile.SqrMoveDistance(manager.worker, targetPosition) < Tile.TestDistance)return true;

				if (path == null || path.Count <= 0)
				{
					path = Pathfinding.GetPath (manager.worker.room, manager.worker, targetPosition);

					if (path == null || path.Count <= 0)
					{
						// No path

						Debug.LogWarning ("No path to target");
						return true;
					}
				}

				Vector2 target = path[0].GetTrueLocation ();
				float distance = Vector2.Distance (manager.worker.transform.position, target);

				if (distance < Tile.TestDistance)
				{
					currentTile = path[0];
					path.RemoveAt (0);

					manager.worker.debugPath = path;

					if (path.Count == 0) return true;

					if (!Pathfinding.IsStandable (path[0]))
					{
						path = null;
						return false;
					}

					return false;
				}

				manager.worker.transform.position = Vector3.Lerp (manager.worker.transform.position, target, manager.worker.movementSpeed * deltaTime / distance);

				return false;
			},

			onFinish: (manager) =>
			{
				manager.worker.debugPath = null;
			},

			onSuccess: onSuccess);
	}

	public static Task PickUp (IResourceInteractable pickUpFrom, ItemType type, TaskSuccessful onSuccess = null)
	{
		if (pickUpFrom == null) return Wait (0.1f);

		float pickUpTimer = 0;

		return new Task (
				onStart: (manager) =>
				{
					if (!pickUpFrom.CanTake (type, manager.worker))
					{
						manager.InterruptTask (Wait (resourceWaitForRetryTime));
						return;
					}

					if (Tile.SqrMoveDistance (manager.worker, pickUpFrom.Location) > Tile.TestDistance)
					{
						manager.InterruptMoveTo (pickUpFrom.Location);
						return;
					}

					if (!pickUpFrom.Interact (manager.worker))
					{
						manager.InterruptTask (Wait (resourceWaitForRetryTime));
						return;
					}

					pickUpTimer = 0;
				},

				onPause: (manager) =>
				{
					pickUpFrom.FinishInteraction (manager.worker);
					pickUpTimer = 0;
				},

				onUpdate: (manager, deltaTime) =>
				{
					if (pickUpTimer < resourceInteractionDelay)
					{
						manager.worker.UpdateDirection (pickUpFrom.FaceDirection);

						pickUpTimer += deltaTime;

						return false;
					}
					else
					{
						pickUpFrom.FinishInteraction (manager.worker);
						return true;
					}
				},

				onFinish: (manager) =>
				{
					pickUpFrom.FinishInteraction (manager.worker);
					if (pickUpFrom.TakeItem (type)) manager.worker.heldItem = type;
				},
				
				onSuccess: onSuccess);
	}

	public static Task Drop (IResourceInteractable dropTarget, TaskSuccessful onSuccess = null)
	{
		if (dropTarget == null) return Wait (0.1f);

		float pickUpTimer = 0;

		return new Task (
				onStart: (manager) =>
				{
					if (!dropTarget.CanDrop(manager.worker.heldItem, manager.worker))
					{
						manager.InterruptTask (Wait (resourceWaitForRetryTime));
						return;
					}

					if (!Tile.IsAtPoint (manager.worker, dropTarget.Location))
					{
						manager.InterruptMoveTo (dropTarget.Location);
						return;
					}

					if (!dropTarget.Interact (manager.worker))
					{
						manager.InterruptTask (Wait(resourceWaitForRetryTime));
						return;
					}

					pickUpTimer = 0;
				},

				onPause: (manager) =>
				{
					dropTarget.FinishInteraction (manager.worker);
					pickUpTimer = 0;
				},

				onUpdate: (manager, deltaTime) =>
				{
					if (pickUpTimer < resourceInteractionDelay)
					{
						manager.worker.UpdateDirection (dropTarget.FaceDirection);

						pickUpTimer += deltaTime;

						return false;
					}
					else
					{
						dropTarget.FinishInteraction (manager.worker);
						return true;
					}
				},

				onFinish: (manager) =>
				{
					dropTarget.FinishInteraction (manager.worker);
					if (dropTarget.GiveItem (manager.worker.heldItem)) manager.worker.heldItem = null;
				},

				onSuccess: onSuccess);
	}

	public static Task Deliver (IResourceInteractable source, IResourceInteractable target, ItemType type, TaskSuccessful onSuccess = null)
	{
		if (source == null || target == null) return Wait(0.1f);

		bool completed = false;

		return new Task (
				onStart: (manager) =>
				{
					if (!completed)
					{
						if (manager.worker.heldItem == null)
						{
							manager.InterruptTask (PickUp (source, type));
							return;
						}
						else
						{
							if (manager.worker.heldItem == type)
							{
								manager.InterruptTask (Drop (target, () =>
								{
									completed = true;
								}));
								return;
							}
							else
							{
								throw new System.Exception ("Worker already holding " + manager.worker.heldItem.name + " so cant pick up " + type.name);
							}
						}
					}
				},

				onPause: Nothing,

				onUpdate: QuickUpdate,

				onFinish: Nothing,

				onSuccess: onSuccess);
	}

	public static Task BuyItem (IResourceInteractable dropTarget, ItemType type, TaskSuccessful onSuccess = null)
	{
		if (dropTarget == null) return Wait (0.1f);

		bool completed = false;
		InOutPort collectLocation = null;

		return new Task (
				onStart: (manager) =>
				{
					if (!completed)
					{
						if (manager.worker.heldItem == null)
						{
							if (collectLocation == null)
							{
								collectLocation = manager.worker.room.GetClosestInOutPort (manager.worker, InOutType.In);
							}

							manager.InterruptTask (PickUp (collectLocation, type));
							return;
						}
						else
						{
							if (manager.worker.heldItem == type)
							{
								manager.InterruptTask (Drop (dropTarget, () =>
								{
									completed = true;
								}));
								return;
							}
							else
							{
								throw new System.Exception ("Worker already holding " + manager.worker.heldItem.name + " so cant pick up " + type.name);
							}
						}
					}
				},

				onPause: (manager) =>
				{
					collectLocation = null;
				},

				onUpdate: QuickUpdate,

				onFinish: Nothing,

				onSuccess: onSuccess);
	}

	public static Task SellItem (IResourceInteractable source, ItemType type, TaskSuccessful onSuccess = null)
	{
		if (source == null) return Wait (0.1f);

		bool completed = false;
		InOutPort dropTarget = null;

		return new Task (
			onStart: (manager) =>
			{
				if (!completed)
				{
					if (manager.worker.heldItem == null)
					{
						manager.InterruptTask (PickUp (source, type));
						return;
					}
					else
					{
						if (manager.worker.heldItem == type)
						{
							if (dropTarget == null)
							{
								dropTarget = manager.worker.room.GetClosestInOutPort (manager.worker, InOutType.Out);
							}

							manager.InterruptTask (Drop (dropTarget, () =>
							{
								completed = true;
							}));
							return;
						}
						else
						{
							throw new System.Exception ("Worker already holding " + manager.worker.heldItem.name + " so cant pick up " + type.name);
						}
					}
				}
			},

			onPause: (manager) =>
			{
				dropTarget = null;
			},

			onUpdate: QuickUpdate,

			onFinish: Nothing,

			onSuccess: onSuccess);
	}

	public static Task Wait (float timeDelay, TaskSuccessful onSuccess = null)
	{
		float timer = 0;

		return new Task (
				onStart: (manager) =>
				{
					timer = 0;
				},

				onPause: Nothing,

				onUpdate: (manager, deltaTime) =>
				{
					if (timer < timeDelay)
					{
						timer += deltaTime;
						return false;
					}

					return true;
				},

				onFinish: Nothing,

				onSuccess: onSuccess);
	}

	public static Task Method (TaskSuccessful onSuccess)
	{
		return new Task (
			onStart: (manager) =>
			{

			},

			onPause: (manager) =>
			{

			},

			onUpdate: (manager, deltaTime) =>
			{
				return true;
			},

			onFinish: (manager) =>
			{

			},

			onSuccess: onSuccess);
	}

	//return new Task (
	//			onStart: (manager) =>
	//			{

	//			},

	//			onPause: (manager) =>
	//			{

	//			},

	//			onUpdate: (manager, deltaTime) =>
	//			{
	//				return true;
	//			},

	//			onFinish: (manager) =>
	//			{

	//			},

	//			onSuccess: onSuccess);

	public static TaskAction Nothing
	{
		get
		{
		 return (manager) => {};
		}
	}

	public static TaskCondition QuickUpdate
	{
		get
		{
			return (manager, deltaTime) => {return true;  };
		}
	}

	#endregion
	#region Base Task Class

	private bool started = false;

	private TaskAction onStart;
	private TaskAction onPause;
	private TaskCondition onUpdate;
	private TaskAction onFinish;

	private TaskSuccessful onSuccess;

	public Task (TaskAction onStart, TaskAction onPause, TaskCondition onUpdate, TaskAction onFinish, TaskSuccessful onSuccess)
	{
		this.onStart = onStart;
		this.onPause = onPause;
		this.onUpdate = onUpdate;
		this.onFinish = onFinish;
		this.onSuccess = onSuccess;
	}

	public void Update (TaskManager manager, float deltaTime)
	{
		if (!started) Start (manager);

		if (onUpdate(manager, deltaTime))
		{
			started = false;

			onFinish (manager);

			if (onSuccess != null) onSuccess ();

			if (!manager.interupted) manager.NextTask ();
		}
	}

	public void Start (TaskManager manager)
	{
		if (!started)
		{
			started = true;
			onStart (manager);
		}
	}

	public void Pause (TaskManager manager)
	{
		if (started)
		{
			started = false;
			onPause (manager);
		}
	}

	#endregion
}

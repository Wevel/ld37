﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate Sprite GetWorkerSprite (Direction dir);
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate Sprite GetTileSprite (TileType type, EdgeType edgeTypeX, EdgeType edgeTypeY);
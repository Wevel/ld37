﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBank : MonoBehaviour
{
	private static PlayerBank instance;

	public static char CurrencySymbol
	{
		get
		{
			return instance.currencySymbol;
		}
	}

	public static bool CanBuy (MachineType machineType) 
	{
		return instance.canBuy (machineType);
	}

	public static bool TrySpend (uint amount)
	{
		return instance.trySpend (amount);
	}

	public static bool CanBuy (ItemType type)
	{
		return instance.canBuy (type);
	}

	public static bool BuyItem (ItemType type)
	{
		return instance.buyItem (type);
	}

	public static void SellItem (ItemType type)
	{
		instance.sellItem (type);
	}

	public static void SellMachine (MachineType type)
	{
		instance.currentMoney += type.sellCost;
	}

	public char currencySymbol;
	public uint startingMoney = 10000;

	public Text currentMoneyText;
	public Text currencySymbolText;

	public ProductionManager productionManager;

	private uint currentMoney;

	void Awake ()
	{
		instance = this;

		currentMoney = startingMoney;

		currencySymbolText.text = currencySymbol + "";
	}

	void Update ()
	{
		currentMoneyText.text = currentMoney + "";
	}

	private bool canSpend (uint amount)
	{
		return currentMoney >= amount;
	}

	private bool trySpend (uint amount)
	{
		if (canSpend (amount))
		{
			currentMoney -= amount;
			return true;
		}

		return false;
	}

	private bool canBuy (MachineType type)
	{
		return canSpend (type.buyCost);
	}

	private bool buyMachine (MachineType type)
	{
		return trySpend (type.buyCost);
	}

	private bool canBuy (ItemType type)
	{
		return canSpend (type.buyCost);
	}

	private bool buyItem (ItemType type)
	{
		return trySpend (type.buyCost);
	}

	private void sellItem (ItemType type)
	{
		// This might not be this simple, there may be modifiered on this

		productionManager.soldItem (type);

		currentMoney += type.sellCost;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPosition
{
	Tile GetTileLocation ();
	Vector2 GetTrueLocation ();
}

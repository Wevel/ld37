﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineTypesLoader : MonoBehaviour
{
	private static Dictionary<string, MachineType> machineTypes = new Dictionary<string, MachineType> ();

	public static IEnumerable<MachineType> MachineTypes
	{
		get
		{
			foreach (KeyValuePair<string, MachineType> item in machineTypes)
			{
				yield return item.Value;
			}
		}
	}

	public static MachineType GetMachineType (string key)
	{
		if (machineTypes.ContainsKey (key)) return machineTypes[key];
		return null;
	}

	public static void LoadIMachines (List<IconDisplayType> sprites)
	{
		machineTypes.Clear ();

		AddMachineType (
			"Laser Cutter",
			"laser_cutter",
			1000, 
			800,
			0.4f,
			new Node (2, 1),
			new Node (0, -1),
			new Node (1, -1) , 
			sprites);

		AddMachineType (
			"Chip Former",
			"chip_former",
			2000,
			1750,
			0.8f,
			new Node (1, 1),
			new Node (-1, 0),
			new Node (1, 0),
			sprites);

		AddMachineType (
			"CNC Mill",
			"cnc_mill",
			1500,
			1300,
			0.8f,
			new Node (1, 2),
			new Node (-1, 0),
			new Node (1, 1),
			sprites);

		AddMachineType (
			"Pick & Place",
			"pick_place",
			3500,
			3100,
			0.8f,
			new Node (2, 2),
			new Node (0, 2),
			new Node (2, 0),
			sprites);
	}

	private static MachineType AddMachineType (string name, string key, uint buyCost, uint sellCost, float craftTimeModifier, Node size, Node inputOffset, Node outputOffset, List<IconDisplayType> sprites)
	{
		if (machineTypes.ContainsKey (key))
		{
			Debug.LogWarning ("Already a machine type with key: " + key);
			return null;
		}

		IconDisplayType display = sprites.Find (x => x.key == key);

		if (display == null)
		{
			Debug.LogWarning ("No sprite with key: " + key);
			return null;
		}

		if ((inputOffset.x >= 0 && inputOffset.x < size.x) &&
			(inputOffset.y >= 0 && inputOffset.y < size.y))
		{
			Debug.LogWarning ("Input offset is inside of the machine for " + name);
			return null;
		}

		if ((outputOffset.x >= 0 && outputOffset.x < size.x) &&
			(outputOffset.y >= 0 && outputOffset.y < size.y))
		{
			Debug.LogWarning ("Ouput offset is inside of the machine for " + name);
			return null;
		}

		MachineType newType = new MachineType (name, key, buyCost, sellCost, craftTimeModifier, size, inputOffset, outputOffset, display.sprite);
		machineTypes.Add (key, newType);

		return newType;
	}
}

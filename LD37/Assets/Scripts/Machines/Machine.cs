﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour, IPosition
{
	public Room room { get; private set; }
	public MachineType type { get; private set; }
	public Tile tile { get; private set; }

	private SpriteRenderer sr;

	private SpriteRenderer inPositionDisplay;
	private SpriteRenderer outPositionDisplay;

	public MachinePort inputPort { get; private set; }
	public MachinePort ouputPort { get; private set; }

	private List<ItemType> storedItems = new List<ItemType> ();

	public Recipe currentRecipe;

	private float productionTimer = 0;

	public bool buyInputs = false;
	public bool sellOutputs = false;

	private SpriteRenderer neededItemDisplay;

	float changeIconTimer = 0;
	float changeIconDelay = 1;

	ItemType currentNeededItem;

	public bool IsUnderMachine (Tile tile)
	{
		return tile.x >= this.tile.x && tile.x < this.tile.x + type.size.x &&
			tile.y >= this.tile.y && tile.y < this.tile.y + type.size.y;
	}

	void Start ()
	{
		neededItemDisplay = new GameObject ("NeededItemDisplay").AddComponent<SpriteRenderer> ();
		neededItemDisplay.transform.SetParent (transform);
		neededItemDisplay.transform.localPosition = new Vector3 (0, 0.5f, 0);

		neededItemDisplay.sortingOrder = 30;

		neededItemDisplay.gameObject.SetActive (false);
	}

	void Update ()
	{
		if (currentRecipe != null)
		{
			if (!HasItem (currentRecipe.product))
			{
				bool needItem = false;

				foreach (ItemType item in currentRecipe.Ingredients)
				{
					if (!HasItem (item)) needItem = true;
				}

				if (!needItem)
				{
					neededItemDisplay.gameObject.SetActive (false);

					if (productionTimer < currentRecipe.craftTime)
					{
						if (MainMenu.Playing)
							productionTimer += Time.deltaTime * room.timeScale * type.craftTimeModifier;
					}
					else
					{
						foreach (ItemType item in currentRecipe.Ingredients)
						{
							storedItems.Remove (item);
						}

						//SoundManager.Play (SoundFxType.Craft);

						storedItems.Add (currentRecipe.product);

						productionTimer = 0;
					}
				}
				else
				{
					productionTimer = 0;
				}

				if (currentNeededItem == null || changeIconTimer > changeIconDelay)
				{
					List<ItemType> needed = NeededItems ();

					if (needed != null && needed.Count > 0)
					{
						int currentIndex = currentNeededItem != null ? needed.IndexOf (currentNeededItem) : -1;
						currentIndex++;

						currentNeededItem = needed[currentIndex % needed.Count];
						neededItemDisplay.sprite = currentNeededItem.sprite;
						neededItemDisplay.gameObject.SetActive (true);
					}
					else
					{
						neededItemDisplay.gameObject.SetActive (false);
					}

					changeIconTimer = 0;
				}
				else
				{
					if (MainMenu.Playing)
						changeIconTimer += Time.deltaTime;

					neededItemDisplay.gameObject.SetActive (true);

					if (HasItem (currentNeededItem))
					{
						currentNeededItem = null;
					}
				}
			}
			else
			{
				productionTimer = 0;
			}
		}
	}

	internal void Sell ()
	{
		room.machineManager.RemoveMachine (this);
		PlayerBank.SellMachine (type);
		Destroy (this.gameObject);
	}

	public void Init (Room room, Tile tile, MachineType type)
	{
		sr = GetComponent<SpriteRenderer> ();
		sr.sprite = type.sprite;

		this.room = room;
		this.tile = tile;
		this.type = type;

		inputPort = new MachinePort (this, room[tile.x + type.inputOffset.x, tile.y + type.inputOffset.y], InOutType.In);
		ouputPort = new MachinePort (this, room[tile.x + type.outputOffset.x, tile.y + type.outputOffset.y], InOutType.Out);
	}

	public Tile GetTileLocation ()
	{
		return tile;
	}

	public Vector2 GetTrueLocation ()
	{
		return transform.position;
	}

	public void ShowInOutPositions (Sprite sprite, Color inColour, Color outColour)
	{
		if (inPositionDisplay == null)
		{
			inPositionDisplay = new GameObject ("InPositionDisplay").AddComponent<SpriteRenderer> ();
			inPositionDisplay.sortingOrder = 5;

			inPositionDisplay.transform.SetParent (transform);
			inPositionDisplay.transform.localPosition = new Vector3 (type.inputOffset.x, type.inputOffset.y);
		}

		if (outPositionDisplay == null)
		{
			outPositionDisplay = new GameObject ("OutPositionDisplay").AddComponent<SpriteRenderer> ();
			outPositionDisplay.sortingOrder = 5;

			outPositionDisplay.transform.SetParent (transform);
			outPositionDisplay.transform.localPosition = new Vector3 (type.outputOffset.x, type.outputOffset.y);
		}

		inPositionDisplay.gameObject.SetActive (true);
		outPositionDisplay.gameObject.SetActive (true);

		inPositionDisplay.sprite = sprite;
		inPositionDisplay.color = inColour;

		outPositionDisplay.sprite = sprite;
		outPositionDisplay.color = outColour;
	}

	public void HideInOutPositions ()
	{
		inPositionDisplay.gameObject.SetActive (false);
		outPositionDisplay.gameObject.SetActive (false);
	}

	public bool GiveItem (ItemType type)
	{
		if (currentRecipe.IsIngredient (type))
		{
			storedItems.Add (type);

			return true;
		}

		return false;
	}

	public bool TakeItem (ItemType type)
	{
		int i = storedItems.FindIndex (x => x.key == type.key);

		if (i < 0) return false;

		storedItems.RemoveAt (i);

		return true;
	}

	public bool HasItem (ItemType type)
	{
		return storedItems.Find (x => x.key == type.key) != null;
	}

	public bool CanTake (ItemType type, Worker worker)
	{
		if(currentRecipe == null) return HasItem (type);
		return HasItem (type) && currentRecipe.product == type;
	}

	public bool CanStore (ItemType type, Worker worker)
	{
		if (currentRecipe == null) return false;

		return currentRecipe.IsIngredient (type);
	}

	public bool CompletedRecipe ()
	{
		if (currentRecipe == null) return false;
		return HasItem (currentRecipe.product);
	}

	public List<ItemType> NeededItems ()
	{
		List<ItemType> needed = new List<ItemType> ();

		if (currentRecipe != null)
		{
			foreach (ItemType item in currentRecipe.Ingredients)
			{
				if (!HasItem (item)) needed.Add (item);
			}
		}

		return needed;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineType
{
	public string name { get; private set; }
	public string key { get; private set; }
	public uint buyCost { get; private set; }
	public uint sellCost { get; private set; }

	public float craftTimeModifier { get; private set; }

	public Node size { get; private set; }
	public Node inputOffset { get; private set; }
	public Node outputOffset { get; private set; }

	public Sprite sprite { get; private set; }

	public MachineType (string name, string key, uint buyCost, uint sellCost, float craftTimeModifier, Node size, Node inputOffset, Node outputOffset, Sprite sprite)
	{
		this.name = name;
		this.key = key;
		this.buyCost = buyCost;
		this.sellCost = sellCost;
		this.craftTimeModifier = craftTimeModifier;

		this.size = size;
		this.inputOffset = inputOffset;
		this.outputOffset = outputOffset;

		this.sprite = sprite;
	}

	public override string ToString ()
	{
		return name;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachinePort : IResourceInteractable
{
	public Machine machine { get; private set; }
	public Tile standTile { get; private set; }
	public InOutType type { get; private set; }
	public Direction FaceDirection { get; private set; }

	private Worker currentWorkerInteraction = null;
	
	public MachinePort (Machine machine, Tile standTile, InOutType type)
	{
		this.machine = machine;
		this.standTile = standTile;
		this.type = type;

		Tile tile = machine.room[standTile.x - 1, standTile.y];
		if (tile != null && machine.IsUnderMachine (tile))
		{
			FaceDirection = Direction.Left;
		}
		else
		{
			tile = machine.room[standTile.x, standTile.y + 1];
			if (tile != null && machine.IsUnderMachine (tile))
			{
				FaceDirection = Direction.Up;
			}
			else
			{
				tile = machine.room[standTile.x + 1, standTile.y];
				if (tile != null && machine.IsUnderMachine (tile))
				{
					FaceDirection = Direction.Right;
				}
				else
				{
					tile = machine.room[standTile.x, standTile.y - 1];
					if (tile != null && machine.IsUnderMachine (tile))
					{
						FaceDirection = Direction.Down;
					}
					else
					{
						FaceDirection = 0;

						Debug.Log (type + " machine port not next to machine");
					}
				}
			}
		}
	}

	public IPosition Location
	{
		get
		{
			return standTile;
		}
	}

	public bool HasWorkerInteracting ()
	{
		return currentWorkerInteraction != null;
	}

	public bool Interact (Worker worker)
	{
		if (currentWorkerInteraction == null)
		{
			currentWorkerInteraction = worker;
			return true;
		}

		return currentWorkerInteraction == worker;
	}

	public void FinishInteraction (Worker worker)
	{
		if (currentWorkerInteraction == worker) currentWorkerInteraction = null;
	}

	public bool GiveItem (ItemType itemType)
	{
		switch (type)
		{
			case InOutType.In:
				return machine.GiveItem (itemType);
			case InOutType.Out:
				Debug.LogWarning ("Cant give items to an input");
				break;
		}

		return false;
	}

	public bool TakeItem (ItemType itemType)
	{
		switch (type)
		{
			case InOutType.In:
				Debug.LogWarning ("Cant take items to an output");
				break;
			case InOutType.Out:
				return machine.TakeItem (itemType);
		}

		return false;
	}

	public bool HasItem (ItemType itemType)
	{
		return machine.HasItem (itemType);
	}

	public bool CanTake (ItemType itemType, Worker worker)
	{
		if (currentWorkerInteraction != null)
		{
			if (currentWorkerInteraction != worker) return false;
		}

		return machine.CanTake (itemType, worker);
	}

	public bool CanDrop (ItemType itemType, Worker worker)
	{
		if (currentWorkerInteraction != null)
		{
			if (currentWorkerInteraction != worker) return false;
		}

		return machine.CanStore (itemType, worker);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineManager
{
	private Transform machineHolder;

	public List<Machine> machines = new List<Machine> ();

	public Room room { get; private set; }

	public MachineManager (Room room)
	{
		this.room = room;

		machineHolder = new GameObject ("MachineHolder").transform;
		machineHolder.transform.position = Vector3.zero;
	}

	public Machine GetMachine (Tile tile)
	{
		foreach (Machine item in machines)
		{
			for (int x = item.tile.x; x < item.tile.x + item.type.size.x; x++)
			{
				for (int y = item.tile.y; y < item.tile.y + item.type.size.y; y++)
				{
					if (room[x, y] == tile) return item;
				}
			}
		}

		return null;
	}

	public Machine PlaceMachine (Room room, Tile tile, MachineType type, Material material)
	{
		Machine machine = new GameObject ("Machine_" + tile.x + "_" + tile.y).AddComponent<Machine> ();
		machine.transform.SetParent (machineHolder);
		machine.transform.position = tile.GetTrueLocation ();

		SpriteRenderer sr = machine.gameObject.AddComponent<SpriteRenderer> ();
		sr.sprite = type.sprite;
		sr.material = material;
		sr.sortingOrder = 25;

		machine.Init (room, tile, type);

		Tile tmpTile;

		for (int x = tile.x; x < tile.x + type.size.x; x++)
		{
			for (int y = tile.y; y < tile.y + type.size.y; y++)
			{
				tmpTile = room[x, y];

				if (tmpTile == null || tmpTile.type != TileType.Floor)
					throw new System.Exception ("Place Machine was called for " + type.name + " at " + tile + " but couldnt actualy be place here");

				tmpTile.SetType (TileType.Covered);
			}
		}

		// Input tile
		tmpTile = room[tile.x + type.inputOffset.x, tile.y + type.inputOffset.y];

		if (tmpTile == null || tmpTile.type == TileType.Wall || tmpTile.type == TileType.Covered)
			throw new System.Exception ("Place Machine was called for " + type.name + " at " + tile + " but couldnt actualy be place here as input is invalid");

		tmpTile.SetType (TileType.MachineInOut);

		// Output tile

		tmpTile = room[tile.x + type.outputOffset.x, tile.y + type.outputOffset.y];

		if (tmpTile == null || tmpTile.type == TileType.Wall || tmpTile.type == TileType.Covered)
			throw new System.Exception ("Place Machine was called for " + type.name + " at " + tile + " but couldnt actualy be place here as output is invalid");

		tmpTile.SetType (TileType.MachineInOut);

		machines.Add (machine);

		return machine;
	}

	public void RemoveMachine (Machine machine)
	{
		Tile tmpTile;

		for (int x = machine.tile.x; x < machine.tile.x + machine.type.size.x; x++)
		{
			for (int y = machine.tile.y; y < machine.tile.y + machine.type.size.y; y++)
			{
				tmpTile = room[x, y];

				if (tmpTile != null)
					tmpTile.SetType (TileType.Floor);

			}
		}

		tmpTile = room[machine.tile.x + machine.type.inputOffset.x, machine.tile.y + machine.type.inputOffset.y];
		if (tmpTile != null)
			tmpTile.SetType (TileType.Floor);

		tmpTile = room[machine.tile.x + machine.type.outputOffset.x, machine.tile.y + machine.type.outputOffset.y];
		if (tmpTile != null)
			tmpTile.SetType (TileType.Floor);

		machines.Remove (machine);
	}
}

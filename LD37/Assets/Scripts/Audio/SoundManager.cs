﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

	private static SoundManager instance;

	public static void Play (SoundFxType type)
	{
		instance.play (type);
	}

	public AudioSource musicSource;
	public AudioSource fxSourcePrefab;

	public float playDelay = 10;

	private float playDelayTimer = 0;

	public AudioClip errorSound;
	public AudioClip dropSound;
	public AudioClip finishCraft;
	public AudioClip newWorker;

	public Toggle playSoundToggle;

	void Awake ()
	{
		DontDestroyOnLoad (gameObject);

		playDelayTimer = playDelay;

		instance = this;
	}

	void Update ()
	{
		if (playSoundToggle.isOn)
		{
			if (musicSource.isPlaying)
			{
				playDelayTimer = 0;
			}
			else
			{
				if (playDelayTimer < playDelay)
				{
					playDelayTimer += Time.deltaTime;
				}
				else
				{
					musicSource.Play ();
				}
			}
		}
		else
		{
			playDelayTimer = playDelay;
			if (musicSource.isPlaying) musicSource.Stop ();
		}
	}

	private void play (SoundFxType type)
	{
		if (!playSoundToggle.isOn) return;

		AudioSource source = Instantiate (fxSourcePrefab);
		source.clip = getClip (type);

		if (source.clip == null)
		{
			Destroy (source.gameObject);
		}
		else
		{
			source.Play ();
			Destroy (source.gameObject, source.clip.length + 0.1f);
		}
	}

	AudioClip getClip (SoundFxType type)
	{
		switch (type)
		{
			case SoundFxType.Error:
				return errorSound;
			case SoundFxType.Drop:
				return dropSound;
			case SoundFxType.Craft:
				return finishCraft;
			case SoundFxType.NewWorker:
				return newWorker;
			default:
				return null;
		}
	}
}

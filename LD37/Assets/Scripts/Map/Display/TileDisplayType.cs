﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileDisplayType
{
	public Sprite sprite;
	public TileType type = TileType.Floor;
	public EdgeType edgeTypeX = EdgeType.Middle;
	public EdgeType edgeTypeY = EdgeType.Middle;
}

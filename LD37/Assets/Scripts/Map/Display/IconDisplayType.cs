﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconDisplayType
{
	public string key { get; private set; }
	public Sprite sprite { get; private set; }

	public IconDisplayType (string key, Sprite sprite)
	{
		this.key = key;
		this.sprite = sprite;
	}
}

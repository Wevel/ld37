﻿[System.Flags]
public enum TileType
{
	Floor = 0,
	Wall = 1,
	Input = 2 + Wall,
	Output = 4 + Wall,
	Door = 8 + Wall,

	Covered = 16,
	MachineInOut = 32,
}

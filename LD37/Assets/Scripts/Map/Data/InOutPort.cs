﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InOutPort : IResourceInteractable
{
	public Tile standTile { get; private set; }	
	public InOutType type { get; private set; }
	public Direction FaceDirection { get; private set; }

	private Worker currentWorkerInteraction = null;

	public InOutPort (Tile standTile, InOutType type, Direction FaceDirection)
	{
		this.standTile = standTile;
		this.type = type;

		this.FaceDirection = FaceDirection;
	}

	public IPosition Location
	{
		get
		{
			return standTile;
		}
	}

	public bool HasWorkerInteracting ()
	{
		return currentWorkerInteraction != null;
	}

	public bool Interact (Worker worker)
	{
		if (currentWorkerInteraction == null)
		{
			currentWorkerInteraction = worker;
			return true;
		}

		return currentWorkerInteraction == worker;
	}

	public void FinishInteraction (Worker worker)
	{
		if (currentWorkerInteraction == worker)
		{
			currentWorkerInteraction = null;
		}
	}

	public bool GiveItem (ItemType itemType)
	{
		switch (type)
		{
			case InOutType.In:
				Debug.LogWarning ("Cant give items to an input");
				break;
			case InOutType.Out:
				//Debug.Log ("Outputted " + itemType.key + " need to deal with this properly (give player money)");

				PlayerBank.SellItem (itemType);

				return true;
		}

		return false;
	}

	public bool TakeItem (ItemType itemType)
	{
		switch (type)
		{
			case InOutType.In:
				//Debug.Log ("Inputted " + itemType.key + " need to deal with this properly (spend player money)");
				return PlayerBank.BuyItem (itemType);
			case InOutType.Out:
				Debug.LogWarning ("Cant take items to an outpu");
				break;
		}

		return false;
	}

	public bool HasItem (ItemType itemType)
	{
		switch (type)
		{
			case InOutType.In:
				return true;
			case InOutType.Out:
				return false;
		}

		return false;
	}

	public bool CanTake (ItemType itemType, Worker worker)
	{
		if (currentWorkerInteraction != null)
		{
			if (currentWorkerInteraction != worker) return false;
		}

		switch (type)
		{
			case InOutType.In:
				return PlayerBank.CanBuy(itemType);
			case InOutType.Out:
				return false;
		}

		return false;
	}

	public bool CanDrop (ItemType itemType, Worker worker)
	{
		if (currentWorkerInteraction != null)
		{
			if (currentWorkerInteraction != worker) return false;
		}

		switch (type)
		{
			case InOutType.In:
				return false;
			case InOutType.Out:
				return true;
		}

		return false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
	public int width { get; private set; }
	public int height { get; private set; }

	public WorkerManager workerManager { get; private set; }
	public MachineManager machineManager { get; private set; }

	private Tile doorSpawnTile;
	private Direction spawnDirection;

	private Tile[,] tiles;

	private List<InOutPort> inOutPorts = new List<InOutPort> ();

	public float timeScale = 1;

	private int lastTaskAssignmentItem = 0;

	public Tile this[int x, int y]
	{
		get
		{
			if (!InRoom (x, y)) return null;
			return tiles[x, y];
		}
	}

	public Room (int width, int height, int numInputs, int numOutputs)
	{
		this.width = width;
		this.height = height;

		tiles = new Tile[width, height];

		List<Tile> possibleINOUT = new List<Tile> ();

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				tiles[x, y] = new Tile (x, y, this);

				if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
				{
					tiles[x, y].SetType (TileType.Wall);

					if ((x > 0 && x < width - 1) || (y > 0 && y < height - 1)) possibleINOUT.Add (tiles[x, y]);
				}
			}
		}

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				tiles[x, y].GenerateNeighbours ();
			}
		}

		if (numInputs + numOutputs > width + width + height + height - 11)
		{
			Debug.Log ("Too maney inputs and outputs for room size");
			return;
		}

		Tile tile = possibleINOUT[Random.Range (0, possibleINOUT.Count)];
		possibleINOUT.Remove (tile);

		tile.SetType (TileType.Door);

		doorSpawnTile = tile;

		if (tile.x == 0)
		{
			//doorSpawnTile = this[1, tile.y];
			spawnDirection = Direction.Right;
		}
		else if (tile.y == 0)
		{
			//doorSpawnTile = this[tile.x, 1];
			spawnDirection = Direction.Up;
		}
		else if (tile.x == width - 1)
		{
			//doorSpawnTile = this[width - 2, tile.y];
			spawnDirection = Direction.Left;
		}
		else if (tile.y == height - 1)
		{
		//	doorSpawnTile = this[tile.x, height - 2];
			spawnDirection = Direction.Down;
		}
		else
			Debug.Log ("No valid door spawn tile for door at " + tile);

		for (int i = 0; i < numInputs; i++)
		{
			tile = possibleINOUT[Random.Range (0, possibleINOUT.Count)];
			possibleINOUT.Remove (tile);

			tile.SetType (TileType.Input);

			inOutPorts.Add (new InOutPort (WallStandTile (tile), InOutType.In, WallFaceDirection (tile)));
		}

		for (int i = 0; i < numOutputs; i++)
		{
			tile = possibleINOUT[Random.Range (0, possibleINOUT.Count)];
			possibleINOUT.Remove (tile);

			tile.SetType (TileType.Output);

			inOutPorts.Add (new InOutPort (WallStandTile (tile), InOutType.Out, WallFaceDirection (tile)));
		}

		workerManager = new WorkerManager ();
		machineManager = new MachineManager (this);
	}

	public GameObject GenerateDisplay (Transform parent, Material material, GetTileSprite getTileSprite)
	{
		GameObject go = new GameObject ("Room");
		go.transform.SetParent (parent);
		go.transform.position = Vector3.zero;
		
		GameObject tileGO;
		SpriteRenderer sr;
		
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				tileGO = new GameObject ("Tile_" + x + "_" + y);
				tileGO.transform.SetParent (go.transform);
				tileGO.transform.position = new Vector3 (x, y, 0);

				sr = tileGO.AddComponent<SpriteRenderer> ();
				sr.sprite = getTileSprite (tiles[x, y].type, getEdgeType(x, width), getEdgeType(y, height));
				sr.material = material;
			}
		}

		return go;
	}

	private EdgeType getEdgeType (int value, int max)
	{
		if (value <= 0) return EdgeType.Min;
		if (value >= max - 1) return EdgeType.Max;
		return EdgeType.Middle;
	}

	public bool InRoom (int x, int y)
	{
		return x >= 0 && x < width && y >= 0 && y < height;
	}

	public Worker SpawnWorker (GetWorkerSprite getWorkerSprite, Material material, float movementSpeed)
	{
		return workerManager.SpawnWorker (this, doorSpawnTile, spawnDirection, getWorkerSprite, material, getWorkerTasks, movementSpeed);
	}

	public Machine PlaceMachine (Tile tile, string typeKey, Material material)
	{
		MachineType type = MachineTypesLoader.GetMachineType (typeKey);

		if (typeKey == null)
		{
			Debug.LogWarning ("No machine with key " + typeKey);
			return null;
		}

		if (!CanPlaceMachine (tile, type))
		{
			Debug.LogWarning ("Cant place " + type.name + " at " + tile);
			return null;
		}

		return PlaceMachine (tile, type, material);
	}

	public Machine PlaceMachine (Tile tile, MachineType type, Material material)
	{
		if (!CanPlaceMachine (tile, type))
		{
			SoundManager.Play (SoundFxType.Error);
			return null;
		}

		if (!PlayerBank.TrySpend (type.buyCost))
		{
			SoundManager.Play (SoundFxType.Error);
			return null;
		}

		SoundManager.Play (SoundFxType.Drop);

		return machineManager.PlaceMachine (this, tile, type, material);
	}

	public bool CanPlaceMachine (Tile tile, string typeKey)
	{
		MachineType type = MachineTypesLoader.GetMachineType (typeKey);

		if (typeKey == null)
		{
			Debug.LogWarning ("No machine with key " + typeKey);
			return false;
		}

		return CanPlaceMachine (tile, type);
	}

	public bool CanPlaceMachine (Tile tile, MachineType type)
	{
		if (tile == null) return false;
		if (!PlayerBank.CanBuy (type)) return false;

		Tile tmpTile;

		for (int x = tile.x; x < tile.x + type.size.x; x++)
		{
			for (int y = tile.y; y < tile.y + type.size.y; y++)
			{
				tmpTile = this[x, y];

				if (tmpTile == null || tmpTile.type != TileType.Floor) return false;
			}
		}

		// Input tile
		tmpTile = this[tile.x + type.inputOffset.x, tile.y + type.inputOffset.y];

		if (tmpTile == null || tmpTile.type == TileType.Wall || tmpTile.type == TileType.Covered) return false;

		// Output tile
		tmpTile = this[tile.x + type.outputOffset.x, tile.y + type.outputOffset.y];

		if (tmpTile == null || tmpTile.type == TileType.Wall || tmpTile.type == TileType.Covered) return false;

		return true;
	}

	private Task[] getWorkerTasks (Worker worker)
	{
		if (machineManager.machines.Count > 0)
		{
			lastTaskAssignmentItem++;
			lastTaskAssignmentItem = lastTaskAssignmentItem % machineManager.machines.Count;

			if (machineManager.machines[lastTaskAssignmentItem].CompletedRecipe ())
			{
				if (machineManager.machines[lastTaskAssignmentItem].sellOutputs)
				{
					return new Task[]
					{
						Task.SellItem(machineManager.machines[lastTaskAssignmentItem].ouputPort, machineManager.machines[lastTaskAssignmentItem].currentRecipe.product),
					};
				}
				else
				{
					return GetMoveToRandomCloseTileTasks (worker);
				}
			}
			else
			{
				List<ItemType> needed = machineManager.machines[lastTaskAssignmentItem].NeededItems ();
				ItemType toGet = null;

				Machine sourceMachine = null;

				foreach (ItemType item in needed)
				{
					sourceMachine = machineManager.machines.Find (x => x.CanTake(item, worker));

					if (sourceMachine != null)
					{
						toGet = item;
						break;
					}
					else
					{
						if (ItemTypesLoader.IsRawResource(item) || machineManager.machines[lastTaskAssignmentItem].buyInputs)
						{
							toGet = item;
							break;
						}
					}						
				}

				if (toGet != null)
				{
					if (sourceMachine != null)
					{
						return new Task[]
						{
							Task.Deliver(sourceMachine.ouputPort, machineManager.machines[lastTaskAssignmentItem].inputPort, toGet),
						};
					}
					else
					{
						return new Task[]
						{
							Task.BuyItem(machineManager.machines[lastTaskAssignmentItem].inputPort, toGet),
						};
					}					
				}
				else
				{
					return GetMoveToRandomCloseTileTasks (worker);
				}	
			}			
		}
		else
		{
			return GetMoveToRandomCloseTileTasks (worker);
		}		
	}

	private Task[] GetMoveToRandomCloseTileTasks (Worker worker)
	{
		Tile randomTarget = GetRandomCloseTile (worker);

		if (randomTarget != null && Pathfinding.IsStandable (randomTarget))
		{
			return new Task[] { Task.MoveTo (randomTarget) };
		}
		else
		{
			return new Task[] { Task.Wait (0.1f) };
		}
	}

	public Tile GetRandomCloseTile (Worker worker)
	{
		Tile baseTile = worker.GetTileLocation ();

		return this[Mathf.Clamp (baseTile.x + Random.Range (-5, 5), 0, width), Mathf.Clamp (baseTile.y + Random.Range (-5, 5), 0, width)];
	}

	public InOutPort GetClosestInOutPort (IPosition currentPosition, InOutType type)
	{
		float bestSqrDistance = Mathf.Infinity;
		float tmpSqrDistance;

		InOutPort bestPort = null;

		foreach (InOutPort item in inOutPorts)
		{
			if (item.type == type)
			{
				if (!item.HasWorkerInteracting ())
				{
					tmpSqrDistance = Tile.SqrMoveDistance (currentPosition, item.standTile);
					if (bestPort == null || tmpSqrDistance < bestSqrDistance)
					{
						bestPort = item;
						bestSqrDistance = tmpSqrDistance;
					}
				}
			}
		}

		if (bestPort == null)
		{
			Debug.LogWarning ("No " + type + " port to go to");
			return null;
		}

		return bestPort;
	}

	public Direction WallFaceDirection (Tile tile)
	{
		if (tile == null) return 0;

		if (tile.x == 0)
		{
			return Direction.Left;
		}
		else if (tile.y == 0)
		{
			return Direction.Down;
		}
		else if (tile.x == width - 1)
		{
			return Direction.Right;
		}
		else if (tile.y == height - 1)
		{
			return Direction.Up;
		}
		else
			Debug.Log ("tile isnt at edge " + tile);

		return 0;
	}

	public Tile WallStandTile (Tile tile)
	{
		return tile;
		//if (tile == null) return null;

		//if (tile.x == 0)
		//{
		//	return this[1, tile.y];
		//}
		//else if (tile.y == 0)
		//{
		//	return this[tile.x, 1];
		//}
		//else if (tile.x == width - 1)
		//{
		//	return this[width - 2, tile.y];
		//}
		//else if (tile.y == height - 1)
		//{
		//	return this[tile.x, height - 2];
		//}
		//else
		//	Debug.Log ("tile isnt at edge " + tile);

		//return tile;
	}

	public Tile GetTile (Vector2 position)
	{
		return this[Mathf.RoundToInt (position.x), Mathf.RoundToInt (position.y)];
	}
}

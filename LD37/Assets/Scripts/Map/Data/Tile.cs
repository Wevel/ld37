﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : IPosition
{
	public const float TestDistance = 0.01f;

	public int x { get; private set; }
	public int y { get; private set; }

	public TileType type { get; private set; }

	public Room room { get; private set; }

	public IEnumerable<Tile> Neighbours
	{
		get
		{
			if (neighbours.ContainsKey (Direction.Left)) yield return neighbours[Direction.Left];
			if (neighbours.ContainsKey (Direction.Up)) yield return neighbours[Direction.Up];
			if (neighbours.ContainsKey (Direction.Right)) yield return neighbours[Direction.Right];
			if (neighbours.ContainsKey (Direction.Down)) yield return neighbours[Direction.Down];
		}
	}

	private Dictionary<Direction, Tile> neighbours = new Dictionary<Direction, Tile> ();

	public Tile this[Direction dir]
	{
		get
		{
			if (neighbours.ContainsKey (dir)) return neighbours[dir];
			return null;
		}
	}

	public Tile (int x, int y, Room room)
	{
		this.x = x;
		this.y = y;
		this.room = room;

		type = TileType.Floor;

	}

	public void GenerateNeighbours ()
	{
		neighbours.Clear ();

		Tile tile;

		tile = room[x - 1, y];
		if (tile != null) neighbours.Add (Direction.Left, tile);

		tile = room[x + 1, y];
		if (tile != null) neighbours.Add (Direction.Right, tile);

		tile = room[x, y - 1];
		if (tile != null) neighbours.Add (Direction.Down, tile);
		tile = room[x, y + 1];
		if (tile != null) neighbours.Add (Direction.Up, tile);

	}

	public void SetType (TileType type)
	{
		this.type = type;
	}

	public Tile GetTileLocation ()
	{
		return this;
	}

	public Vector2 GetTrueLocation ()
	{
		float xValue = x;
		float yValue = y;

		if (x == 0)
			xValue += 0.2f;
		else if (x == room.width - 1)
			xValue -= 0.2f;

		if (y == 0)
			yValue += 0.49f;
		else if (y == room.height - 1)
			yValue -= 0f;

		return new Vector3 (xValue, yValue);
	}

	public override string ToString ()
	{
		return x + "," + y;
	}

	public static int ManhattenDistnace (IPosition a, IPosition b)
	{
		Tile aTile = a.GetTileLocation ();
		Tile bTile = b.GetTileLocation ();

		return Mathf.Abs (aTile.x - bTile.x) + Mathf.Abs (aTile.y - bTile.y);
	}

	public static float MoveDistance (IPosition a, IPosition b)
	{
		return Vector2.Distance (a.GetTrueLocation (), b.GetTrueLocation ());
	}

	public static float SqrMoveDistance (IPosition a, IPosition b)
	{
		return (a.GetTrueLocation () - b.GetTrueLocation ()).sqrMagnitude;
	}

	public static Direction GetDirection (IPosition a, IPosition b)
	{
		Tile aTile = a.GetTileLocation ();
		Tile bTile = b.GetTileLocation ();

		Direction dir = 0;

		if (aTile.x < bTile.x)
			dir = dir | Direction.Right;
		else if (aTile.x > bTile.x)
			dir = dir | Direction.Left;

		if (aTile.y < bTile.y)
			dir = dir | Direction.Up;
		else if (aTile.y > bTile.y)
			dir = dir | Direction.Down;

		return dir;
	}

	public static Direction GetDirection (Vector2 delta)
	{
		Direction dir = 0;

		if (delta.x < 0)
			dir = dir | Direction.Left;
		else if (delta.x > 0)
			dir = dir | Direction.Right;

		if (delta.y < 0)
			dir = dir | Direction.Down;
		else if (delta.y > 0)
			dir = dir | Direction.Up;

		return dir;
	}

	public static bool IsAtPoint (IPosition a, IPosition b)
	{
		return SqrMoveDistance (a, b) < TestDistance;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pathfinding
{
	private class PathNode
	{
		public bool closed = false;
		public float fScore;
		public float gScore;
		public PathNode parent;

		public Tile tile { get; private set; }

		public PathNode (Tile tile)
		{
			this.tile = tile;
		}
	}

	public static bool IsStandable (Tile tile)
	{
		return tile.type != TileType.Covered;
	}

	private static float Distance (Tile a, Tile b)
	{
		return Tile.ManhattenDistnace (a, b);
	}

	private static float EstimateDistance (Tile a, Tile target)
	{
		return Distance (a, target);
	}

	public static List<Tile> GetPath (Room room, IPosition currentPosition, IPosition targetPosition)
	{
		if (room == null || currentPosition == null || targetPosition == null)
		{
			throw new System.Exception ("Invalid get path args");
		}

		Tile start = currentPosition.GetTileLocation ();
		Tile target = targetPosition.GetTileLocation ();

		if (!IsStandable (target))
		{
			return new List<Tile> ();
		}

		int closedNodes = 0;

		List<PathNode> nodes = new List<PathNode> ();
		nodes.Add (new PathNode (start));

		float tgScore;

		float bestSqrDistance = Mathf.Infinity;
		float tmpSqrDistance;

		PathNode currentTile = null;
		PathNode existingTile;

		while (nodes.Count > closedNodes)
		{
			currentTile = null;

			foreach (PathNode item in nodes)
			{
				if (!item.closed)
				{
					tmpSqrDistance = Tile.SqrMoveDistance (currentPosition, item.tile);
					if (currentTile == null || tmpSqrDistance < bestSqrDistance)
					{
						currentTile = item;
						bestSqrDistance = tmpSqrDistance;
					}
				}				
			}

			if (currentTile.tile == target) break;

			currentTile.closed = true;
			closedNodes++;


			foreach (Tile item in currentTile.tile.Neighbours)
			{
				if (IsStandable (item))
				{
					if ((existingTile = nodes.Find (x => x.tile == item)) != null)
					{
						if (existingTile.closed) continue;

						tgScore = currentTile.gScore + Distance (currentTile.tile, item);

						if (existingTile.gScore > tgScore) continue;
					}
					else
					{
						tgScore = currentTile.gScore + Distance (currentTile.tile, item);

						existingTile = new PathNode (item);

						nodes.Add (existingTile);
					}

					existingTile.parent = currentTile;
					existingTile.gScore = tgScore;
					existingTile.fScore = tgScore + EstimateDistance (item, target);
				}
			}
		}

		if (currentTile.tile != target)
		{
			// Didnt find path

			Debug.LogWarning ("Couldn't find path from " + start + " to " + target);
			return new List<Tile> ();
		}

		List<Tile> path = new List<Tile> ();

		path.Add (currentTile.tile);

		while ((currentTile = currentTile.parent) != null) path.Add (currentTile.tile);

		path.Reverse ();

		return path;
	}
}

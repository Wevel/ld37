﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControl : MonoBehaviour
{
	public int roomWidth = 12;
	public int roomHeight = 10;

	public int roomInputCount = 3;
	public int roomOutputCount = 3;

	public float meanWorkerSpeed = 4;
	public float workerSpeedVariation = 0.25f;

	public float timeScale = 1;

	public Material roomDisplayMaterial;

	public Room room { get; private set; }

	public List<TileDisplayType> tileSprites = new List<TileDisplayType> ();

	public string resourceIconLocation;
	public string machineIconLocation;

	private List<IconDisplayType> iconSprites = new List<IconDisplayType> ();
	private List<IconDisplayType> machineSprites = new List<IconDisplayType> ();

	public Sprite workerSpriteUp;
	public Sprite workerSpriteRight;
	public Sprite workerSpriteDown;
	public Sprite workerSpriteLeft;

	public Material workerMaterial;
	public Material machineMaterial;

	public Sprite errorSprite;

	public uint startingHireCost = 1500;
	public uint hireCostChange = 250;

	private uint currentHireCost;

	void Awake ()
	{
		currentHireCost = startingHireCost;

		room = new Room (roomWidth, roomHeight, roomInputCount, roomOutputCount);
		room.GenerateDisplay (transform, roomDisplayMaterial, GetTileSprite);

		// Items
		Sprite[] iconSpritesArray = Resources.LoadAll<Sprite> (resourceIconLocation);

		for (int i = 0; i < iconSpritesArray.Length; i++)
		{
			iconSprites.Add (new IconDisplayType (iconSpritesArray[i].name.Substring (14), iconSpritesArray[i]));
		}

		ItemTypesLoader.LoadItems (iconSprites);

		//Machines
		Sprite[] machineSpritesArray = Resources.LoadAll<Sprite> (machineIconLocation);

		for (int i = 0; i < machineSpritesArray.Length; i++)
		{
			machineSprites.Add (new IconDisplayType (machineSpritesArray[i].name.Substring (15), machineSpritesArray[i]));
		}

		MachineTypesLoader.LoadIMachines (machineSprites);

		room.SpawnWorker (GetWorkerSprite, workerMaterial, meanWorkerSpeed + Random.Range (-workerSpeedVariation, workerSpeedVariation));
	}

	void Update ()
	{
		if (timeScale < 0) timeScale = 0;

		room.timeScale = timeScale;
	}

	private Sprite GetTileSprite (TileType type, EdgeType edgeTypeX, EdgeType edgeTypeY)
	{
		TileDisplayType displayType = tileSprites.Find (x => x.type == type && x.edgeTypeX == edgeTypeX && x.edgeTypeY == edgeTypeY);

		if (displayType != null) return displayType.sprite;

		Debug.Log ("Couldnt find display type for: " + type + " X edge: " + edgeTypeX + " Y edge: " + edgeTypeY);

		return errorSprite;
	}

	private Sprite GetWorkerSprite (Direction dir)
	{
		switch (dir)
		{
			case Direction.Up:
				return workerSpriteUp;
			case Direction.Right:
				return workerSpriteRight;
			case Direction.Down:
				return workerSpriteDown;
			case Direction.Left:
				return workerSpriteLeft;
			default:

				if ((dir & Direction.Up) != 0) return workerSpriteUp;
				if ((dir & Direction.Down) != 0) return workerSpriteDown;
				if ((dir & Direction.Left) != 0) return workerSpriteLeft;
				if ((dir & Direction.Right) != 0) return workerSpriteRight;
				break;
		}

		return errorSprite;
	}

	public void TryHireWorker ()
	{
		if (PlayerBank.TrySpend (currentHireCost))
		{
			room.SpawnWorker (GetWorkerSprite, workerMaterial, meanWorkerSpeed + Random.Range (-workerSpeedVariation, workerSpeedVariation));

			SoundManager.Play (SoundFxType.NewWorker);

			currentHireCost += hireCostChange;
		}
		else
		{
			SoundManager.Play (SoundFxType.Error);
		}
	}

	public uint NextHireCost ()
	{
		return currentHireCost;
	}
}

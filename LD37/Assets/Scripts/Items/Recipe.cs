﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recipe
{
	private List<ItemType> ingredients;

	public IEnumerable<ItemType> Ingredients
	{
		get
		{
			foreach (ItemType item in ingredients)
			{
				yield return item;
			}
		}
	}

	public ItemType product { get; private set; }

	public float craftTime { get; private set; }

	public string craftMachineKey { get; private set; }

	public Recipe (ItemType[] ingredients, ItemType product, float craftTime, string craftMachineKey)
	{
		this.ingredients = new List<ItemType> (ingredients);
		this.product = product;
		this.craftTime = craftTime;
		this.craftMachineKey = craftMachineKey;
	}

	public bool CheckGotIngredients (HasItem checkHasItem)
	{
		foreach (ItemType item in ingredients)
		{
			if (!checkHasItem (item)) return false;
		}

		return true;
	}

	public void SpawnProduct (CreateItem onCreate)
	{
		onCreate (product);
	}

	public bool IsIngredient (ItemType type)
	{
		return ingredients.Find (x => x.key == type.key) != null;
	}
}

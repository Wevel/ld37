﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemType
{
	public string name { get; private set; }
	public string key { get; private set; }
	public uint buyCost { get; private set; }
	public uint sellCost { get; private set; }

	public Sprite sprite { get; private set; }
	public List<Recipe> recipes = new List<Recipe> ();

	public ItemType (string name, string key, uint buyCost, uint sellCost, Sprite sprite)
	{
		this.name = name;
		this.key = key;
		this.buyCost = buyCost;
		this.sellCost = sellCost;

		this.sprite = sprite;
	}

	public void AddRecipe (Recipe recipe)
	{
		if (!recipes.Contains (recipe)) recipes.Add (recipe);
	}

	public override string ToString ()
	{
		return name;
	}
}

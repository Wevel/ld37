﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IResourceInteractable
{
	IPosition Location
	{
		get;
	}

	Direction FaceDirection
	{
		get;
	}

	bool Interact (Worker worker);
	void FinishInteraction (Worker worker);

	bool GiveItem (ItemType itemType);
	bool TakeItem (ItemType itemType);
	bool HasItem (ItemType itemType);
	bool CanTake (ItemType itemType, Worker worker);
	bool CanDrop (ItemType itemType, Worker worker);
}
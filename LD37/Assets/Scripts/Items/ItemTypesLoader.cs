﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ItemTypesLoader
{
	private static Dictionary<string, ItemType> itemTypes = new Dictionary<string, ItemType> ();

	public static List<Recipe> FindPossibleRecipies (MachineType machine)
	{
		List<Recipe> recipes = new List<Recipe> ();

		foreach (KeyValuePair<string, ItemType> item in itemTypes)
		{
			foreach (Recipe recipe in item.Value.recipes)
			{
				if (machine.key == recipe.craftMachineKey) recipes.Add (recipe);
			}
		}

		return recipes;
	}

	public static ItemType GetItemType (string key)
	{
		if (itemTypes.ContainsKey (key)) return itemTypes[key];
		return null;
	}

	public static ItemType GetRandomItemType ()
	{
		List<string> types = new List<string> (itemTypes.Keys);

		return GetItemType (types[Random.Range (0, types.Count)]);
	}

	public static bool IsRawResource (ItemType type)
	{
		if (type.key == "silicon") return true;
		if (type.key == "gold") return true;
		if (type.key == "copper") return true;
		if (type.key == "glass_fibre") return true;

		return false;
	}

	public static void LoadItems (List<IconDisplayType> sprites)
	{
		itemTypes.Clear ();

		#region Items

		// Raw resources
		ItemType silicon = AddItemType ("Silicon", "silicon", 6, 4, sprites);
		ItemType gold = AddItemType ("Gold", "gold", 10, 8, sprites);
		ItemType copper = AddItemType ("Copper", "copper", 4, 3, sprites);
		ItemType glassFibre = AddItemType ("Glass Fibre", "glass_fibre", 2, 1, sprites);

		// Intermediary 
		ItemType siliconWafer = AddItemType ("Silicon Wafer", "silicon_wafer", 10, 1, sprites);

		// Chips
		ItemType logicChip = AddItemType ("Logic Chip", "logic_chip", 36, 30, sprites);
		ItemType microProccessor = AddItemType ("Micro Proccessor", "micro_proccessor", 60, 50, sprites);
		ItemType cpu = AddItemType ("CPU", "cpu", 115, 90, sprites);

		// Boards
		ItemType pcb = AddItemType ("PCB", "pcb", 10, 8, sprites);
		ItemType multiLayerBoard = AddItemType ("Multi-Layer Board", "multi_layer_board", 25, 20, sprites);

		ItemType basicCircuit = AddItemType ("Basic Circuit", "basic_circuit", 50, 45, sprites);
		ItemType motherBoard = AddItemType ("Mother Board", "mother_board", 265, 240, sprites);

		#endregion
		#region Recipes

		// -> Intermediary
		AddRecipe (siliconWafer, 1, "laser_cutter", silicon);

		// -> Chips
		AddRecipe (logicChip, 2, "chip_former", siliconWafer, copper);
		AddRecipe (microProccessor, 5, "chip_former", siliconWafer, gold);
		AddRecipe (cpu, 10, "chip_former", siliconWafer, gold, copper);

		// -> Boards
		AddRecipe (pcb, 4, "cnc_mill", glassFibre, copper);
		AddRecipe (multiLayerBoard, 7, "cnc_mill", glassFibre, gold, copper);

		AddRecipe (basicCircuit, 4, "pick_place", logicChip, pcb);
		AddRecipe (motherBoard, 12, "pick_place", cpu, microProccessor, multiLayerBoard);

		#endregion
	}

	private static ItemType AddItemType (string name, string key, uint buyCost, uint sellCost, List<IconDisplayType> sprites)
	{
		if (itemTypes.ContainsKey (key))
		{
			Debug.LogWarning ("Already an item type with key: " + key);
			return null;
		}

		IconDisplayType display = sprites.Find (x => x.key == key);

		if (display == null)
		{
			Debug.LogWarning ("No sprite with key: " + key);
			return null;
		}

		ItemType newType = new ItemType (name, key, buyCost, sellCost, display.sprite);
		itemTypes.Add (key, newType);

		return newType;
	}

	private static void AddRecipe (ItemType product, float craftTime, string craftMachineKey, params ItemType[] ingredients)
	{
		if (product == null)
		{
			Debug.LogWarning ("No product given for recipe");
			return;
		}

		product.AddRecipe (new Recipe(ingredients, product, craftTime, craftMachineKey));
	}
}
